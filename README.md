# `hv_vcpu_interrupt` thread safety POC

This is a hacked-together mini hypervisor that runs some custom live-waiting guest code (in the form of a DOS .COM file) and keeps sending it external interrupts, recording corresponding VM exits.

The hypervisor code is directly based on [Michael Steil's `hvdos` DOS emulator](https://github.com/mist64/hvdos).
There's no specific reason why to use a *DOS* emulator in particular for this project.
This was the only super simple, standalone, hackable, but **working** x86 Hypervisor.framework open source project I could find.
xhyve and Qemu are great, but they are much too complex for my needs - I wanted to run bare-metal assembly without a BIOS and whatnot.

## Why?

The `hv_vcpu_interrupt` function in macOS's Hypervisor.framework is a little under-documented with regard to its edge cases.
In particular, there has been some [question about possible race conditions](https://lists.nongnu.org/archive/html/qemu-devel/2020-06/msg08154.html), especially pondering whether a call to `hv_vcpu_interrupt()` would *always* cause a vCPU exit, no matter what state the vCPU thread is in. So in the absence of clarification from Apple (I asked in a DTS TSI but received no reply) I wanted to empirically test its behaviour.

### What does it do?

3 phases:

 1. Bring up a Hypervisor for running real-mode x86 code, with an MS-DOS-like interrupt interface. (Inherited from hvdos) Start running a custom .COM program (`livewait.com`) which prints a message on startup, loops until a register value changes, then prints another message and exits. The register must be changed from outside the program.
 2. Perform a series of deterministic tests:
    * Self-interrupt: calls `hv_vcpu_interrupt` on the vCPU thread, causing an immediate exit when attempting to run the vCPU.
    * Interrupt from another thread while not running the vCPU. Again, check this causes an exit as soon as the vCPU runs.
    * Interrupt from another thread while running the vCPU, again causing an exit.
 3. Repeatedly interrupt the vCPU from another thread, stochastically. Interrupts are randomly spaced, and the interrupting thread checks whether the previous `hv_vcpu_interrupt` call caused an exit, and how long it took to reach the exit.

### What's the result of the investigation?

As far as I can tell, on macOS 14.3.1:

 * There will always be a vCPU exit very shortly after any `hv_vcpu_interrupt` call(s) (usually &lt;30µs; I have not seen anything longer than 200µs on Coffee Lake).
 * If the vCPU is not running when `hv_vcpu_interrupt` is called, there will be an immediate exit when you attempt to run the vCPU.
 * Multiple shortly-spaced `hv_vcpu_interrupt` calls can be coalesced, but once exited, a subsequent `hv_vcpu_interrupt` will cause a further exit on attempted VM re-entry.
 
In other words, I've been unable to provoke any kind of race condition.
In particular, interrupting the vCPU when it is **not** running is fine, and will always cause a subsequent VM exit when the vCPU thread does attempt to run it.

## How do I build it?

To build the hypervisor, run `make`.
To build the guest code, ensure `nasm` is installed (e.g. from homebrew) and run:

```
nasm -f bin -o livewait.com livewait-com.asm
```

To run the code:

## Example run:

```
% ./hvdos livewait.com
EXIT_REASON_EXCEPTION: interrupt_number = 0x21
Starting random interrupts
VM interrupt 0/100000…
Starting long loop...
VM interrupt 10000/100000…
VM interrupt 20000/100000…
VM interrupt 30000/100000…
VM interrupt 40000/100000…
VM interrupt 50000/100000…
VM interrupt 60000/100000…
VM interrupt 70000/100000…
VM interrupt 80000/100000…
VM interrupt 90000/100000…
interrupts finished
EXIT_REASON_EXCEPTION: interrupt_number = 0x21
Done
EXIT_REASON_EXCEPTION: interrupt_number = 0x21
Status: STOP/UNSUPPORTED
Sent 100000 randomly spaced interrupts, with 100049 interrupt VM exits, max exit delay 69103 ns
```




# Based on: hvdos

*hvdos* is a simple DOS emulator based on the OS X 10.10 Yosemite Hypervisor.framework.

## License

See [LICENSE.txt](LICENSE.txt) (2-clause-BSD).

In order to simplify use of this code as a template, you can consider any parts from "hvdos.c" and "interface.h" as being in the public domain.

## Author

Michael Steil <mist64@mac.com>
