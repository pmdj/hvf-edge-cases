// Copyright (c) 2009-present, the hvdos developers. All Rights Reserved.
// Read LICENSE.txt for licensing information.
//
// hvdos - a simple DOS emulator based on the OS X 10.10 Hypervisor.framework

#include <atomic>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include <sys/time.h>
#include <Hypervisor/hv.h>
#include <Hypervisor/hv_vmx.h>
#include "vmcs.h"
#include "interface.h"
#include "DOSKernel.h"

#define DEBUG 1

using std::atomic;

/* read GPR */
uint64_t
rreg(hv_vcpuid_t vcpu, hv_x86_reg_t reg)
{
	uint64_t v;

	if (hv_vcpu_read_register(vcpu, reg, &v)) {
		abort();
	}

	return v;
}

/* write GPR */
void
wreg(hv_vcpuid_t vcpu, hv_x86_reg_t reg, uint64_t v)
{
	if (hv_vcpu_write_register(vcpu, reg, v)) {
		abort();
	}
}

/* read VMCS field */
static uint64_t
rvmcs(hv_vcpuid_t vcpu, uint32_t field)
{
	uint64_t v;

	if (hv_vmx_vcpu_read_vmcs(vcpu, field, &v)) {
		abort();
	}

	return v;
}

/* write VMCS field */
static void
wvmcs(hv_vcpuid_t vcpu, uint32_t field, uint64_t v)
{
	if (hv_vmx_vcpu_write_vmcs(vcpu, field, v)) {
		abort();
	}
}

/* desired control word constrained by hardware/hypervisor capabilities */
static uint64_t
cap2ctrl(uint64_t cap, uint64_t ctrl)
{
	return (ctrl | (cap & 0xffffffff)) & (cap >> 32);
}

static void* vcpu_thread(void*);
struct vmx_caps
{
	uint64_t pinbased, procbased, procbased2, entry;
};

enum vm_interrupt_test_state
{
	INIT,
	SELF_INTR,
	SELF_INTR_RECEIVED,
	EXITED_OTHER_INTR,
	EXITED_OTHER_INTR_RECEIVED,
	RUNNING_OTHER_INTR,
};

static constexpr uint32_t NUM_RANDOM_INTERRUPTS = 100000;
static constexpr uint32_t MEAN_SQRT_RANDOM_INTERRUPT_INTERVAL_US = 20;

struct vm_state
{
	vmx_caps caps;
	hv_vcpuid_t vcpu;
	void* mem;
	int argc;
	char **argv;
	pthread_mutex_t m;
	pthread_cond_t c;
	vm_interrupt_test_state progress;
	atomic<uint32_t> num_random_interrupts_sent alignas(64);
	atomic<uint64_t> last_interrupt_send_time_nsec;
	atomic<bool> random_interrupts_done;
	uint32_t num_random_interrupts_recvd alignas(64);
	int64_t max_random_interrupt_exit_delay;
};
static void cleanup(vm_state& vm);

static uint64_t gen_random_exp(uint64_t mean)
{
	double u = drand48();
	return static_cast<uint64_t>(round(-log(u) * mean));
}

int
main(int argc, char **argv)
{
	if (argc < 2) {
		fprintf(stderr, "Usage: hvdos [com file]\n");
						exit(1);
	}

	/* create a VM instance for the current task */
	if (hv_vm_create(HV_VM_DEFAULT)) {
		abort();
	}

	/* get hypervisor enforced capabilities of the machine, (see Intel docs) */
	struct vm_state vm = {};
	vm.argc = argc;
	vm.argv = argv;
	vm.progress = INIT;
	pthread_mutex_init(&vm.m, NULL);
	pthread_cond_init(&vm.c, NULL);
	
	if (hv_vmx_read_capability(HV_VMX_CAP_PINBASED, &vm.caps.pinbased)) {
		abort();
	}
	if (hv_vmx_read_capability(HV_VMX_CAP_PROCBASED, &vm.caps.procbased)) {
		abort();
	}
	if (hv_vmx_read_capability(HV_VMX_CAP_PROCBASED2, &vm.caps.procbased2)) {
		abort();
	}
	if (hv_vmx_read_capability(HV_VMX_CAP_ENTRY, &vm.caps.entry)) {
		abort();
	}

	/* allocate some guest physical memory */
#define VM_MEM_SIZE (1 * 1024 * 1024)
	
	if (!(vm.mem = valloc(VM_MEM_SIZE))) {
		abort();
	}
	/* map a segment of guest physical memory into the guest physical address
	 * space of the vm (at address 0) */
	if (hv_vm_map(vm.mem, 0, VM_MEM_SIZE, HV_MEMORY_READ | HV_MEMORY_WRITE
		| HV_MEMORY_EXEC))
	{
		abort();
	}
	
	// Spawn the vCPU in a separate thread
	pthread_mutex_lock(&vm.m);
	pthread_t cpu_thread;
	int status = pthread_create(&cpu_thread, NULL, vcpu_thread, &vm);
	assert(status == 0);
	
	// VM interrupt behaviour test sequence:
	// 1. Wait for vCPU thread to complete self-interrupt test
	while (vm.progress < SELF_INTR_RECEIVED)
	{
		pthread_cond_wait(&vm.c, &vm.m);
	}
	assert(vm.progress == SELF_INTR_RECEIVED);
	vm.progress = EXITED_OTHER_INTR;
	
	// 2. Interrupt halted vCPU and ensure the interrupt was received
	hv_vcpu_interrupt(&vm.vcpu, 1);
	pthread_cond_signal(&vm.c);
	
	while (vm.progress < EXITED_OTHER_INTR_RECEIVED)
	{
		pthread_cond_wait(&vm.c, &vm.m);
	}
	
	assert(vm.progress == EXITED_OTHER_INTR_RECEIVED);
	vm.progress = RUNNING_OTHER_INTR;
	pthread_mutex_unlock(&vm.m);

	// 3. Interrupt running vCPU 1000 times and ensure it resulted in 1000 exits
	srand48(time(nullptr));
	printf("Starting random interrupts\n");
	for (uint32_t i = 0; i < NUM_RANDOM_INTERRUPTS; ++i)
	{
		if (i % 10000 == 0)
			printf("VM interrupt %u/%u…\n", i, NUM_RANDOM_INTERRUPTS);
		uint64_t sleep_us = gen_random_exp(MEAN_SQRT_RANDOM_INTERRUPT_INTERVAL_US);
		sleep_us *= sleep_us;
		if (sleep_us > 100000)
			sleep_us = 100000;
		if (sleep_us > 1)
			usleep(sleep_us);
		uint64_t now = clock_gettime_nsec_np(CLOCK_MONOTONIC_RAW);
		uint64_t prev = 0;
		vm.num_random_interrupts_sent++;
		bool prev_interrupt_handled = vm.last_interrupt_send_time_nsec.compare_exchange_strong(prev, now);
		if (!prev_interrupt_handled)
		{
			timeval time_now = {};
			gettimeofday(&time_now, nullptr);
			pthread_mutex_lock(&vm.m);
			
			prev = 0;
			prev_interrupt_handled = vm.last_interrupt_send_time_nsec.compare_exchange_strong(prev, now);
			if (!prev_interrupt_handled)
			{
				//printf("Interrupt not yet handled after %llu ns…\n", now - prev);
				
				// Try to provoke a long delay if the interrupt did get dropped
				timespec deadline = {};
				deadline.tv_sec = time_now.tv_sec;
				deadline.tv_nsec = time_now.tv_usec * NSEC_PER_USEC;
				deadline.tv_nsec += NSEC_PER_MSEC * 100;
				if (deadline.tv_nsec > NSEC_PER_SEC) {
					++deadline.tv_sec;
					deadline.tv_nsec -= NSEC_PER_SEC;
				}
				pthread_cond_timedwait(&vm.c, &vm.m, &deadline);
			}
			pthread_mutex_unlock(&vm.m);
		}
		hv_vcpu_interrupt(&vm.vcpu, 1);
	}
	
	pthread_mutex_lock(&vm.m);
	vm.random_interrupts_done.store(true);
	pthread_mutex_unlock(&vm.m);
	
	printf("interrupts finished\n");
		
	pthread_join(cpu_thread, NULL);
	
	printf("Sent %u randomly spaced interrupts, with %u interrupt VM exits, max exit delay %lld ns\n",
		vm.num_random_interrupts_sent.load(),
		vm.num_random_interrupts_recvd,
		vm.max_random_interrupt_exit_delay);
	
	cleanup(vm);
	return 0;
}

static void* vcpu_thread(void* opaque)
{
	vm_state& vm = *static_cast<vm_state*>(opaque);
	
	/* create a vCPU instance for this thread */
	hv_vcpuid_t vcpu;
	if (hv_vcpu_create(&vcpu, HV_VCPU_DEFAULT)) {
		abort();
	}

	/* vCPU setup */
#define VMCS_PRI_PROC_BASED_CTLS_HLT           (1 << 7)
#define VMCS_PRI_PROC_BASED_CTLS_CR8_LOAD      (1 << 19)
#define VMCS_PRI_PROC_BASED_CTLS_CR8_STORE     (1 << 20)

	/* set VMCS control fields */
    wvmcs(vcpu, VMCS_PIN_BASED_CTLS, cap2ctrl(vm.caps.pinbased, 0));
    wvmcs(vcpu, VMCS_PRI_PROC_BASED_CTLS, cap2ctrl(vm.caps.procbased,
                                                   VMCS_PRI_PROC_BASED_CTLS_HLT |
                                                   VMCS_PRI_PROC_BASED_CTLS_CR8_LOAD |
                                                   VMCS_PRI_PROC_BASED_CTLS_CR8_STORE));
	wvmcs(vcpu, VMCS_SEC_PROC_BASED_CTLS, cap2ctrl(vm.caps.procbased2, 0));
	wvmcs(vcpu, VMCS_ENTRY_CTLS, cap2ctrl(vm.caps.entry, 0));
	wvmcs(vcpu, VMCS_EXCEPTION_BITMAP, 0xffffffff);
	wvmcs(vcpu, VMCS_CR0_MASK, 0x60000000);
	wvmcs(vcpu, VMCS_CR0_SHADOW, 0);
	wvmcs(vcpu, VMCS_CR4_MASK, 0);
	wvmcs(vcpu, VMCS_CR4_SHADOW, 0);
	/* set VMCS guest state fields */
	wvmcs(vcpu, VMCS_GUEST_CS_SELECTOR, 0);
	wvmcs(vcpu, VMCS_GUEST_CS_LIMIT, 0xffff);
	wvmcs(vcpu, VMCS_GUEST_CS_ACCESS_RIGHTS, 0x9b);
	wvmcs(vcpu, VMCS_GUEST_CS_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_DS_SELECTOR, 0);
	wvmcs(vcpu, VMCS_GUEST_DS_LIMIT, 0xffff);
	wvmcs(vcpu, VMCS_GUEST_DS_ACCESS_RIGHTS, 0x93);
	wvmcs(vcpu, VMCS_GUEST_DS_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_ES_SELECTOR, 0);
	wvmcs(vcpu, VMCS_GUEST_ES_LIMIT, 0xffff);
	wvmcs(vcpu, VMCS_GUEST_ES_ACCESS_RIGHTS, 0x93);
	wvmcs(vcpu, VMCS_GUEST_ES_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_FS_SELECTOR, 0);
	wvmcs(vcpu, VMCS_GUEST_FS_LIMIT, 0xffff);
	wvmcs(vcpu, VMCS_GUEST_FS_ACCESS_RIGHTS, 0x93);
	wvmcs(vcpu, VMCS_GUEST_FS_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_GS_SELECTOR, 0);
	wvmcs(vcpu, VMCS_GUEST_GS_LIMIT, 0xffff);
	wvmcs(vcpu, VMCS_GUEST_GS_ACCESS_RIGHTS, 0x93);
	wvmcs(vcpu, VMCS_GUEST_GS_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_SS_SELECTOR, 0);
	wvmcs(vcpu, VMCS_GUEST_SS_LIMIT, 0xffff);
	wvmcs(vcpu, VMCS_GUEST_SS_ACCESS_RIGHTS, 0x93);
	wvmcs(vcpu, VMCS_GUEST_SS_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_LDTR_SELECTOR, 0);
	wvmcs(vcpu, VMCS_GUEST_LDTR_LIMIT, 0);
	wvmcs(vcpu, VMCS_GUEST_LDTR_ACCESS_RIGHTS, 0x10000);
	wvmcs(vcpu, VMCS_GUEST_LDTR_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_TR_SELECTOR, 0);
	wvmcs(vcpu, VMCS_GUEST_TR_LIMIT, 0);
	wvmcs(vcpu, VMCS_GUEST_TR_ACCESS_RIGHTS, 0x83);
	wvmcs(vcpu, VMCS_GUEST_TR_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_GDTR_LIMIT, 0);
	wvmcs(vcpu, VMCS_GUEST_GDTR_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_IDTR_LIMIT, 0);
	wvmcs(vcpu, VMCS_GUEST_IDTR_BASE, 0);

	wvmcs(vcpu, VMCS_GUEST_CR0, 0x20);
	wvmcs(vcpu, VMCS_GUEST_CR3, 0x0);
	wvmcs(vcpu, VMCS_GUEST_CR4, 0x2000);

	/* initialize DOS emulation */
	DOSKernel Kernel((char *)vm.mem, vcpu, vm.argc, vm.argv);

	/* read COM file at 0x100 */
	FILE *f = fopen(vm.argv[1], "r");
	fread((char *)vm.mem + 0x100, 1, 64 * 1024, f);
	fclose(f);

	/* set up GPRs, start at COM file entry point */
	wreg(vcpu, HV_X86_RIP, 0x100);
	wreg(vcpu, HV_X86_RFLAGS, 0x2);
	wreg(vcpu, HV_X86_RSP, 0x0);

	// 1. self-interrupt test
	pthread_mutex_lock(&vm.m);
	assert(vm.progress == INIT);
	vm.vcpu = vcpu;
	vm.progress = SELF_INTR;
	pthread_mutex_unlock(&vm.m);
	hv_return_t r = hv_vcpu_interrupt(&vcpu, 1);
	if (r != HV_SUCCESS)
		printf("vm_vcpu_interrupt -> 0x%x\n", r);
	

	/* vCPU run loop */
	int stop = 0;
	do {
		if (hv_vcpu_run_until(vcpu, HV_DEADLINE_FOREVER)) {
			abort();
		}
		/* handle VMEXIT */
		uint64_t exit_reason = rvmcs(vcpu, VMCS_EXIT_REASON);
		//printf("VMEXIT: %llu\n", exit_reason);

		switch (exit_reason) {
			case EXIT_REASON_EXCEPTION: {
				uint8_t interrupt_number = rvmcs(vcpu, VMCS_IDT_VECTORING_INFO) & 0xFF;
				printf("EXIT_REASON_EXCEPTION: interrupt_number = 0x%02x\n", interrupt_number);
				int Status = Kernel.dispatch(interrupt_number);
				switch (Status) {
					case DOSKernel::STATUS_HANDLED:
						wreg(vcpu, HV_X86_RIP, rreg(vcpu, HV_X86_RIP) + 2);
						break;
					case DOSKernel::STATUS_UNSUPPORTED:
					case DOSKernel::STATUS_STOP:
						printf("Status: STOP/UNSUPPORTED\n");
						stop = 1;
						break;
					case DOSKernel::STATUS_NORETURN:
						// The kernel changed the PC.
						break;
					default:
						break;
				}
				break;
			}
			case EXIT_REASON_EXT_INTR:
#if DEBUG
				//printf("IRQ (progress: %u)\n", vm.progress);
#endif
				pthread_mutex_lock(&vm.m);
				switch (vm.progress) {
				case SELF_INTR:
					vm.progress = SELF_INTR_RECEIVED;
					pthread_cond_signal(&vm.c);
					
					while (vm.progress == SELF_INTR_RECEIVED)
					{
						pthread_cond_wait(&vm.c, &vm.m);
					}
					assert(vm.progress == EXITED_OTHER_INTR);
					break;
				
				case EXITED_OTHER_INTR:
					vm.progress = EXITED_OTHER_INTR_RECEIVED;
					pthread_cond_signal(&vm.c);
					
					break;
				case RUNNING_OTHER_INTR:
				{
					++vm.num_random_interrupts_recvd;
					uint64_t now = clock_gettime_nsec_np(CLOCK_MONOTONIC_RAW);
					uint64_t sent_time = vm.last_interrupt_send_time_nsec.exchange(0);
					if (sent_time != 0)
					{
						pthread_cond_signal(&vm.c);
						int64_t delta = now - sent_time;
						if (delta > vm.max_random_interrupt_exit_delay)
							vm.max_random_interrupt_exit_delay = delta;
					}
					if (vm.random_interrupts_done.load() && 0x12345678 == rreg(vcpu, HV_X86_RBX))
					{
						wreg(vcpu, HV_X86_RBX, 0);
					}
					break;
				}
				default:
					printf("EXIT_REASON_EXT_INTR in unexpected state %u\n", vm.progress);
					break;
				}
				pthread_mutex_unlock(&vm.m);
				
				break;
			case EXIT_REASON_HLT:
				/* guest executed HLT */
#if DEBUG
				printf("HLT\n");
#endif
				stop = 1;
				break;
			case EXIT_REASON_EPT_FAULT:
				/* disambiguate between EPT cold misses and MMIO */
				/* ... handle MMIO ... */
				break;
	 		/* ... many more exit reasons go here ... */
			default:
				printf("unhandled VMEXIT (%llu)\n", exit_reason);

				stop = 1;
		}
	} while (!stop);

	/*
	 * optional clean-up
	 */

	/* destroy vCPU */
	if (hv_vcpu_destroy(vcpu)) {
		abort();
	}

	return NULL;
}

static void cleanup(vm_state& vm)
{
	/* unmap memory segment at address 0 */
	if (hv_vm_unmap(0, VM_MEM_SIZE)) {
		abort();
	}
	/* destroy VM instance of this task */
	if (hv_vm_destroy()) {
		abort();
	}

	free(vm.mem);
}
