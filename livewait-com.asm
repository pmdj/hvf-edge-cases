ORG 100h

        lea dx, [msg1]
        mov ah,9
        int 21h

				mov ebx, 12345678h
liveloop:
				test ebx, ebx
				; Hypervisor should eventually change ebx
				jnz liveloop

done:
        lea dx, [msg2]
        mov ah,9
        int 21h
        mov ax, 4c00h
        int 21h

msg1: DB `Starting long loop...\n$`
msg2: DB `Done\n$`
